@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="col-sm-offset-2 col-sm-8">
			<div class="panel panel-default">
				<div class="panel-heading">
					New Task
				</div>

				<div class="panel-body">
					<!-- New Task Form -->
					<form action="/task" method="POST" class="form-horizontal" enctype="multipart/form-data">
						{{ csrf_field() }}

						<!-- Task Name -->
						<div class="form-group">
							<label for="task-name" class="col-sm-3 control-label">Task Name :</label>
							<div class="col-sm-6">
								<input type="text" name="name" id="name" class="form-control" value="{{ old('task') }}">
							</div>
						</div>
						
						<!-- Task Image -->
						<div class="form-group">
							<label for="task-image" class="col-sm-3 control-label">Task Image :</label>
							<div class="input-field col-sm-6">
								<input type="file" id="image" name="image" class="validate"/ >
							</div>
						</div>

						<!-- Add Task Button -->
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-6">
								<button type="submit" class="btn btn-default">
									<i class="fa fa-btn fa-plus"></i>Add Task
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>

			<!-- Current Tasks -->
			@if (count($tasks) > 0)
				<div class="panel panel-default">
					<div class="panel-heading">
						<hr>
						Current Tasks
						<form class="form-inline" method="GET">
							<div class="form-group">
								<label for="task-image" class="col-sm-3 control-label">Search</label>
								<div class="input-field col-sm-6">
						
									<input type="text" name="search" value="{{ app('request')->input('search') }}" class="form-control">	
								</div>
							</div>
							<button type="submit" class="btn btn-default">Search</button>
						</form>
						<br>
						<a href="{{url('/tasks/export/pdf')}}" class="btn btn-info">Export PDF</a> 
						<a href="{{url('/tasks/export/excel')}}" class="btn btn-success">Export Excel</a>
					</div>

					<div class="panel-body">
						<table class="table table-striped task-table">
							<thead>
								<th>Task</th>
								<th>Image</th>
								<th>&nbsp;</th>
							</thead>
							<tbody>
								@foreach ($tasks as $task)
									<tr>
										<td class="table-text"><div>{{ $task->name }}</div></td>
										<td class="table-text"><img src="{{asset('/images/'.$task->image)}}" width="200px" class="img img-rounded" alt="Nothing"></td>

										<!-- Task Delete Button -->
										<td>
											<form action="/task/{{ $task->id }}" method="POST">
												{{ csrf_field() }}
												{{ method_field('DELETE') }}

												<button type="submit" id="delete-task-{{ $task->id }}" class="btn btn-danger">
													<i class="fa fa-btn fa-trash"></i>Delete
												</button>
												
												<a href="{{ url('/tasks').'/'.$task->id.'/edit' }}" class="btn btn-warning">Edit</a>
											</form>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			@else
				Data Tidak Ada ->
				<a href="{{url('/tasks')}}">kembali</a>
			@endif
		</div>
	</div>
@endsection
