@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="col-sm-offset-2 col-sm-8">
			<div class="panel panel-default">
				<div class="panel-heading">
					New Task
				</div>

				<div class="panel-body">
					<!-- New Task Form -->
					<form action="{{route('task.update', $task->id)}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
						{{ csrf_field() }}
						{{ method_field('PUT') }}

						<!-- Task Name -->
						<div class="form-group">
							<label for="task-name" class="col-sm-3 control-label">Task</label>

							<div class="col-sm-6">
								<input type="text" name="name" id="task-name" class="form-control" value="{{ $task->name }}">
							</div>
						</div>
						
						<!-- Task Image -->
						<div class="form-group">
							<label for="task-image" class="col-sm-3 control-label">Task Image :</label>
							<div class="input-field col-sm-6">
								<input type="file" id="image" name="image" class="validate"/ >
							</div>
							<div>
								@if($task->image != null)
									<img src="{{asset('/images/'.$task->image)}}" width="200px" class="img img-rounded" alt="Nothing">
								@endif
							</div>
						</div>

						<!-- Add Task Button -->
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-6">
								<button type="submit" class="btn btn-default">
									<i class="fa fa-btn fa-plus"></i>Save Task
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection
