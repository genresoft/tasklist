<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'web'], function () {
	
    // Task Routes
    Route::get('/tasks', 'TaskController@index');
    Route::post('/task', 'TaskController@store');
	Route::get('/tasks/{id}/edit', 'TaskController@edit');
	Route::put('/task/{id}', 'TaskController@update')->name('task.update');
    Route::delete('/task/{id}', 'TaskController@destroy');
    Route::get('/tasks/export/pdf', 'TaskController@exportPDF');
    Route::get('/tasks/export/excel', 'TaskController@exportExcel');

    // Authentication Routes...
    Route::auth();
});
