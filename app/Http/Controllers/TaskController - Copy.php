<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\TaskRepository;
use Illuminate\Support\Facades\File;

class TaskControllerCOPY extends Controller
{	
	public function __construct(TaskRepository $tasks)
	{
		$this->middleware('auth');
	}
	
	public function index(Request $request)
    {
		$user_id = \Auth::user()->id;
        $task = Task::where('user_id', $user_id)->get();
		
		return view('tasks.index', [
            'tasks' => $task,
        ]);
    }
	
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
			'image' => 'required|image',
        ]);
		
		$task = new Task;
		
		if ($request->hasFile('image')) {
			// menambil image yang diupload berikut ekstensinya
            $filename = null;
            $uploaded_image = $request->file('image');
            $extension = $uploaded_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = md5(time()) . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'images';
            // memindahkan file ke folder public/images
            $uploaded_image->move($destinationPath, $filename);
            // ganti field image dengan image yang baru
            $task->image = $filename;
		}
		
		$task->user_id = \Auth::user()->id;
		$task->name = $request->name;
		$task->save();

        return redirect('/tasks');
    }
	
	public function edit($id)
    {
		$task = Task::find($id);
		
        return view('tasks.edit', [
            'task' => $task,
        ]);
    }
	
	public function update(Request $request, $id)
    {
       $this->validate($request, [
            'name' => 'required|max:255',
        ]);
		
		$task = Task::find($id);
		$task->user_id = \Auth::user()->id;
		$task->name = $request->name;
		
		
		if ($request->hasFile('image')) {
            // mengambil image yang diupload berikut ekstensinya
            $filename = null;
            $uploaded_image = $request->file('image');
            $extension = $uploaded_image->getClientOriginalExtension();
            // membuat nama file random dengan extension
            $filename = md5(time()) . '.' . $extension;
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'images';
            // memindahkan file ke folder public/images
            $uploaded_image->move($destinationPath, $filename);
            // hapus image lama, jika ada
            if ($task->image) {
                $old_image = $task->image;
                $filepath = public_path() . DIRECTORY_SEPARATOR . 'images'
                    . DIRECTORY_SEPARATOR . $task->image;
				try {
					File::delete($filepath);
				} catch (FileNotFoundException $e) {
					// File sudah dihapus/tidak ada
				}
            }
            // ganti field image dengan image yang baru
            $task->image = $filename;
        }
		
		
		$task->save();

        return redirect('/tasks');
    }
	
    public function destroy( $id)
    {
        $task = Task::find($id);
        $task->delete();

        return redirect('/tasks');
    }
}
