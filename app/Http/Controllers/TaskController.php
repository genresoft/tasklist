<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\TaskRepository;
use Illuminate\Support\Facades\File;
use PDF;

class TaskController extends Controller
{	
	public function __construct(TaskRepository $tasks)
	{
		$this->middleware('auth');
	}
	
	public function index(Request $request)
    {
		$user_id = \Auth::user()->id;
        $task = Task::query();
        if ($request->has('search')) {
            $param = $request->search;
            $task->where('name', 'like', '%' . $param . '%');
        }
        $task = $task->where('user_id', $user_id)->get();
		return view('tasks.index', ['tasks' => $task,]);
    }
	
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
			'image' => 'required|image',
        ]);
		$task = new Task;
		if ($request->hasFile('image')) {
            $filename = null;
            $upload = $request->file('image');
            $filename = $upload->getClientOriginalName();
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'images';
            $upload->move($destinationPath, $filename);
            $task->image = $filename;
		}
		$task->user_id = \Auth::user()->id;
		$task->name = $request->name;
		$task->save();
        return redirect('/tasks');
    }
	
	public function edit($id)
    {
		$task = Task::find($id);
        return view('tasks.edit', ['task' => $task,]);
    }
	
	public function update(Request $request, $id)
    {
       $this->validate($request, [
            'name' => 'required|max:255',
        ]);
		$task = Task::find($id);
		$task->user_id = \Auth::user()->id;
		$task->name = $request->name;
		if ($request->hasFile('image')) {
            $filename = null;
            $upload = $request->file('image');
            $filename = $upload->getClientOriginalName();
            $destinationPath = public_path() . DIRECTORY_SEPARATOR . 'images';
            $upload->move($destinationPath, $filename);
            if ($task->image) {
                $old_image = $task->image;
                $filepath = public_path() . DIRECTORY_SEPARATOR . 'images'
                    . DIRECTORY_SEPARATOR . $task->image;
				try {
					File::delete($filepath);
				} catch (FileNotFoundException $e) {
					// file sebelumnya kosong
				}
            }
            $task->image = $filename;
        }
		$task->save();
        return redirect('/tasks');
    }
	
    public function destroy( $id)
    {
        $task = Task::find($id);
		 if ($task->image) {
                $old_image = $task->image;
                $filepath = public_path() . DIRECTORY_SEPARATOR . 'images'
                    . DIRECTORY_SEPARATOR . $task->image;
				try {
					File::delete($filepath);
				} catch (FileNotFoundException $e) {
					// file sebelumnya kosong
				}
            }
        $task->delete();
        return redirect('/tasks');
    }

    public function exportPDF()
    {
        $data = Task::get();
        $pdf = PDF::loadView('pdf.task', compact('data'));
        return $pdf->download('task_data_'.date('Y-m-d_H-i-s').'.pdf');
    }

    public function exportExcel()
    {
        $data = Task::get();
        $pdf = Excel::loadView('excel.task', compact('data'));
        return $excel->download('task_data_'.date('Y-m-d_H-i-s').'.excel');
    }
}